# API Docs

not done

## Get poll

```http
  GET /api/getPoll
```

## Create poll

```http
  POST /api/newPoll
```

## Vote on a poll

```http
  POST /api/vote
```

## Edit poll

Currently only used to remove

```http
  POST /api/edit
```

