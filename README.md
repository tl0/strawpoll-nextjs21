# Strawpoll - Yet another strawpoll thingy.

Live site: https://strawpoll.fi/

## Tech Stack

**Client:** Next.js, React, TailwindCSS, SWR, nivo

**Backend:** Node, Next.js, Vercel, mongoose

## Getting Started

First, run the development server:

```bash
git clone this
cd this
yarn # or "yarn install" to install dependencies 
yarn dev # or vercel dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Remember to set .env variables yo.

## Environment Variables
```
STRAWPOLL_DB_HOST=""
STRAWPOLL_DB_USER=""
STRAWPOLL_DB_PASSWD=""
HCAPTCHA_SECRET=""
NEXT_PUBLIC_HCAPTCHA_SITEKEY=""
```
