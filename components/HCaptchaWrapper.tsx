// https://github.com/hCaptcha/react-hcaptcha/issues/31

import React, { useState, useEffect, useRef } from 'react'
import HCaptcha from '@hcaptcha/react-hcaptcha';

const HCaptchaWrapper = ({ id, sitekey, theme, size, onVerify, ref, onError, onExpire }) => {

    const [isMounted, setIsMounted] = useState(false)

    useEffect(() => {
        setIsMounted(true)
    }, [])

    if (isMounted) {
        return <HCaptcha id={id} sitekey={sitekey} theme={theme} size={size} onVerify={onVerify} onError={onError} onExpire={onExpire} ref={ref} />
    }
    else {
        return null
    }
}

export default HCaptchaWrapper
