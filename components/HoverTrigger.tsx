import React, { useEffect, useState, useContext } from 'react';

const ToggleComponent = ({ value, children }) => {
    return (<>
        <div className="relative hover-trigger">
            <span className="px-1 rounded-lg bg-gray-100 border border-gray-300">{value}</span>
            <div className="absolute bg-gray-100 border border-gray-100 px-4 py-2 hover-target">
                {children}
            </div>
        </div>

        <style jsx>{`
    .hover-trigger > span {
        cursor: help;
    }
    .hover-trigger .hover-target {
        position: absolute; 
  left: -999em;
}

.hover-trigger:hover .hover-target {
    display: block;
    position: absolute; 
  left: 0em;
  z-index: 20000 !important;
  overflow: visible;
}
      `}
        </style>
    </>)
}

export default ToggleComponent
