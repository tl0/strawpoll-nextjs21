import { PromiseProvider } from 'mongoose';
import React, { FunctionComponent } from 'react';

type ToggleComponentProps = {
  value: string,
  name: string,
  fref: any
}

const ToggleComponent: FunctionComponent<ToggleComponentProps> = React.forwardRef((props, fref) => {
  return (<>
    <div className="mx-auto">

      <main className="grid p-2">
        <label className="flex items-center relative cursor-pointer select-none">
          <span className="text-lg mr-3">{props.value}</span>
          <div className="mx-auto absolute right-2 flex">
            <input {...props.fref(props.name)} type="checkbox" className="appearance-none transition-colors cursor-pointer w-14 h-7 rounded-full focus:outline-none lg:focus:ring-2 focus:ring-offset-4 focus:ring-offset-gray-400 focus:ring-blue-500" />
            <span className="absolute font-medium text-xs uppercase right-1 self-center">
              OFF
            </span>
            <span className="absolute font-medium text-xs uppercase right-8 self-center">
              ON
            </span>
            <span className="w-7 h-7 absolute rounded-full transition-all bg-gray-200">
            </span>
          </div>
        </label>
      </main>
    </div>


    <style jsx>{`  
  input {
    background-color: #ef4444; /* bg-red-500 */
  }
  
  input:checked {
    background-color: #22c55e; /* bg-green-500 */
  }
  
  input ~ span:last-child {
    right: 1.75rem; /* right-7 */
  }
  
  input:checked ~ span:last-child {
    right: 0; /* right-0 */
  }
      `}
    </style>
  </>)
})

export default ToggleComponent
