import mongoose, { Schema } from 'mongoose'
import * as PrNaGe from 'project-name-generator'

mongoose.connect(`mongodb+srv://${process.env.STRAWPOLL_DB_USER}:${process.env.STRAWPOLL_DB_PASSWD}@${process.env.STRAWPOLL_DB_HOST}?retryWrites=true&w=majority`, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('Connected to DB! :D')
});

const addNewPoll = async ({ question, choises, ip, multiple = false, strictspam = false, captcha = false }) => {
    try {
        let t = new PollModel({
            id: PrNaGe.generate({ words: 2, number: true }).dashed,
            question,
            choises: choises.filter(e => e.trim() !== "").map((v, i) => ({ id: i, text: v, votes: 0 })),
            author: { ip: ip },
            options: {
                captcha,
                multiple,
                strictspam,
            },
        })
        return t.save()
    } catch (err) {
        console.error(err)
        throw new Error('Failed to add!')
    }
}

const getPoll = async ({ id, filter = "-author" }) => {
    try {
        let t = await PollModel.findOne({
            id
        }).select(filter)
        if (!t) {
            return null
        }
        return {
            ...t.toObject(),
            totalVotes: t.choises.reduce((a, v) => {
                return a + v.votes
            }, 0)
        }
    } catch (err) {
        console.error(err)
        return null
    }
}

const getPolls = async () => {
    try {
        let t = await PollModel.find({ lastVoteDate: { "$gte": new Date(new Date().getTime() - 30 * 24 * 60 * 60 * 1000) } }).sort({ lastVoteDate: 'desc' }).limit(420)
        if (!t) {
            return null
        }
        return t
    } catch (err) {
        console.error(err)
        return null
    }
}

const deletePoll = async (pid) => {
    try {
        let t = await PollModel.deleteOne({
            id: pid
        })
        if (!t) {
            return null
        }
        return t
    } catch (err) {
        console.error(err)
        return null
    }
}

const addVote = async ({ id, choise, ip }) => {
    try {
        let t = await PollModel.updateMany({
            id: id,
            //'choises.text': { $in: ['Helsinki','Pori'] }
        }, {
            $inc: { 'choises.$[elem].votes': 1, 'totalVotes': 1 },
            $set: { 'lastVoteDate': Date.now() }
        }, {
            arrayFilters: [{
                'elem.id': { $in: [...choise].map(a => parseInt(a)) }
            }]
        }).exec()
        return t.nModified == 1;
    } catch (e) {
        console.error(e)
    }
}

const pollSchema = new mongoose.Schema({
    id: { type: String, index: true, unique: true, required: true },
    date: { type: Date, default: Date.now },
    lastVoteDate: { type: Date, default: Date.now },
    options: { multiple: Boolean, strictspam: Boolean, captcha: Boolean },
    author: { ip: String },
    question: { type: String, required: true },
    choises: [{
        id: Number,
        text: String,
        img: String,
        votes: { type: Number, default: 0 },
    }],
});

/*pollSchema.virtual('totalVotes').get(function () {
    let tid = this._id
    return PollModel.aggregate([
        {
            $match: {
                "_id": tid,
            },
        },
        {
            $addFields: {
                "totalVotes": {
                    $sum: "$choises.votes",
                }
            }
        }
    ]).then(e => {
        console.log('RETURN')
        let ret = e[0].totalVotes
        console.log(ret)
        return ret
    })
})*/

/*
const pollSchema = new mongoose.Schema({
    id: { type: String, index: true, unique: true, required: true },
    date: { type: Date, default: Date.now },
    lastVoteDate: { type: Date, default: Date.now },
    options: { multiple: Boolean, strictspam: Boolean },
    author: { ip: String },
    question: { type: String, required: true },
    choises: [{
        type: Map,
        of: new Schema({
            id: mongoose.Schema.Types.ObjectId,
            text: String,
            img: String,
            votes: { type: Number, default: 0 },
        }),
    }],
});
*/

let PollModel;
try {
    PollModel = mongoose.model('polls');
} catch (error) {
    PollModel = mongoose.model('polls', pollSchema);
}

export { PollModel, addNewPoll, getPoll, getPolls, addVote, deletePoll }
