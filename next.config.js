const nextTranslate = require('next-translate')

module.exports = nextTranslate({
    future: {
        webpack5: false,
    },
    async rewrites() {
        return [
            {
                source: "/sitemap.xml",
                destination: "/api/sitemap",
            },
        ]
    },
})
