import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import '../styles/tailwind.css'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  return <>
    <Head>
      <title key="title">Strawpoll.fi - Yet another poll thingy.</title>
      <meta name="description" content="Yet another poll thingy." key="description" />
      <meta name="keywords" content="strawpoll, straw, poll, voting, vote, quick poll, online poll, poll maker, poll creator, make a poll, question, opinion, finland" key="keywords" />
      <link
        rel="icon"
        key="icon"
        type="image/svg+xml"
        href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'><text y='.9em' font-size='90'>📊</text></svg>"
      />
    </Head>
    <Component {...pageProps} />
    <div className="fixed top-2 right-2">
      {router.locales.map(e => router.locale !== e && <Link href={router.asPath} locale={e} prefetch={false} replace={true} key={e}><a><Image src={`/flags/${e}.png`} width={30} height={17} alt={e.toUpperCase()} /></a></Link>)}
    </div>
    <script defer src='https://static.cloudflareinsights.com/beacon.min.js' data-cf-beacon='{"token": "a15924d15b9b43a08df643dc9348daa6", "spa": true}'></script>
  </>
}

export default MyApp
