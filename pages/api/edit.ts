import { getPoll, deletePoll } from '../../database/pollModel'

export default async function handler(req, res) {
    let r = req.body
    let q = req.query
    let ip = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress
    let h = req.headers
    let t

    if (req.method === 'POST') {
        try {
            t = await getPoll({ id: r.id, filter: "" });
            if (t.author?.ip == ip) {
                console.log("Access granted!")
                if (r.action == "delete") {
                    let d = await deletePoll(r.id)
                    if (d) {
                        res.status(200).json({ result: 'rm ok' })
                    } else {
                        res.status(500).json({ result: 'rm not ok' })
                    }
                } else {
                    res.status(200).json({ result: 'OK?' })
                }
            } else {
                console.error("No access!")
                res.status(403).json({ result: 'no access' })
            }
        } catch (err) {
            console.error(err)
            res.status(500).send('error')
        }
    } else {
        res.status(405).send('error')
    }
}

