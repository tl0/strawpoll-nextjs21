import { getPoll } from '../../database/pollModel'

export default async function handler(req, res) {
    let r = req.query
    let t
    let id = r.id

    if (req.method === 'GET') {
        try {
            t = await getPoll({
                id,
            });
            res.setHeader('Cache-Control', 'Cache-Control: public, max-age=1, s-maxage=2, stale-while-revalidate')
            res.status((t !== null) ? 200 : 404).json(t)
        } catch (err) {
            console.error(err)
            res.status(500).send('error')
        }
    } else {
        res.status(405).send('error')
    }
}

