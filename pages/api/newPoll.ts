import { addNewPoll } from '../../database/pollModel'

export default async function handler(req, res) {
    let r = req.body
    let ip = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress
    let h = req.headers
    let t

    if (req.method === 'POST') {
        try {
            t = await addNewPoll({
                question: r.question,
                choises: r.choises,
                ip,
                multiple: r.multiple,
                strictspam: r.strictspam,
                captcha: r.captcha,
            });
            res.status(200).json({ result: 'OK', id: t.id, t, r, h })
        } catch (err) {
            console.error(err)
            res.status(500).send('error')
        }
    } else {
        res.status(405).send('error')
    }
}

