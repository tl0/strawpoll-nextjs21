import { SitemapStream, streamToPromise } from "sitemap"
import { getPolls } from "../../database/pollModel";

export default async (req, res) => {
    const polls = await getPolls()

    const smStream = new SitemapStream({
        hostname: "https://strawpoll.fi",
    });

    smStream.write({
        url: "/",
        priority: 1,
        changefreq: "weekly",
        links: [
            {
                lang: 'x-default',
                url: '/en',
            },
            {
                lang: 'fi',
                url: '/',
            },
            {
                lang: 'en',
                url: '/en',
            },
        ],
    })

    polls.forEach(e => {
        smStream.write({
            url: `/r/${e.id}`,
            lastmod: e.lastVoteDate.toISOString(),
            priority: 0.6,
            changefreq: "daily",
            links: [
                {
                    lang: 'x-default',
                    url: `/en/r/${e.id}`,
                },
                {
                    lang: 'fi',
                    url: `/r/${e.id}`,
                },
                {
                    lang: 'en',
                    url: `/en/r/${e.id}`,
                },
            ],
        })
        smStream.write({
            url: `/p/${e.id}`,
            lastmod: e.lastVoteDate.toISOString(),
            priority: 0.4,
            changefreq: "daily",
            links: [
                {
                    lang: 'x-default',
                    url: `/en/p/${e.id}`,
                },
                {
                    lang: 'fi',
                    url: `/p/${e.id}`,
                },
                {
                    lang: 'en',
                    url: `/en/p/${e.id}`,
                },
            ],
        })
    })
    smStream.end()
    const sitemap = await streamToPromise(smStream).then((sm) => sm.toString())
    res.setHeader("Content-Type", "text/xml")
    res.write(sitemap)
    res.end()
}
