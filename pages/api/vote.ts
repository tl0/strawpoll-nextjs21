import { addVote, getPoll } from '../../database/pollModel'
import { verify } from 'hcaptcha'

export default async function handler(req, res) {
    let r = req.body
    let ip = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for']
    let h = req.headers
    let t;
    let result;

    try {
        console.log(r)
        let poll = await getPoll({ id: r.id })
        if (poll.options.captcha) {
            verify(process.env.HCAPTCHA_SECRET, r.captcha)
                .then(async (data) => {
                    result = await addVote({ id: r.id, choise: r.choise, ip: ip })
                })
                .catch(console.error);
        } else {
            result = await addVote({ id: r.id, choise: r.choise, ip: ip })
        }
    } catch (err) {
        console.error(err)
    }
    if (req.method === 'POST') {
        res.status(200).json({ result: result ? 'OK' : 'FAILED' })
    } else {
        res.status(405).send('error')
    }
}

