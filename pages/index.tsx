import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import useTranslation from 'next-translate/useTranslation'
import Trans from 'next-translate/Trans'
import styles from '../styles/Home.module.css'

import NewPollComponent from '../components/NewPoll'

export default function Home({ locale }) {
  const router = useRouter()
  const { t, lang } = useTranslation('home')

  return (
    <div className={styles.container}>
      <Head>
        <title>{t('h1')} - {t('h2')}</title>
        <meta name="description" content={t('h2')} key="description" />
        <link rel="canonical" href={`https://strawpoll.fi/${router.locale !== router.defaultLocale ? router.locale : ''}`} />
        <link rel="alternate" hrefLang="x-default" href="https://strawpoll.fi/en" />
        <link rel="alternate" hrefLang="fi" href="https://strawpoll.fi/" />
        {router.locales.map(e => e !== router.defaultLocale && <link rel="alternate" hrefLang={e} href={`https://strawpoll.fi/${e}`} key={e} />)}
      </Head>

      <main className={styles.main}>
        <div className="w-full h-screen bg-no-repeat bg-fixed bg-cover blur bg-bottom bg-gradient-to-r from-blue-600 to-red-500" style={{ "backgroundImage": 'url("https://source.unsplash.com/qqE7qERX7Go/1920x1080")' }}>
          <div className="w-full bg-opacity-50 dark:bg-opacity-80 bg-black flex justify-center items-center backdrop-blur-[3px] backdrop-filter">
            <div className="w-full h-screen">
              <div className="mx-4 text-center text-white dark:text-gray-300 lg:mt-16">
                <h1 className="font-bold text-6xl mb-4">{t('h1')}</h1>
                <h2 className="font-bold text-3xl mb-12">{t('h2')}</h2>
                <div>
                  <Link href="/r/demo-1234">
                    <a className="bg-gray-500 rounded-md font-bold text-white dark:text-gray-300 text-center px-4 py-3 transition duration-300 ease-in-out hover:bg-gray-600 mr-2">
                      Example poll
                    </a>
                  </Link>
                </div>
              </div>
              <NewPollComponent />
            </div>
          </div>
          <footer className="fixed bottom-1 left-2 text-gray-400 text-sm">
            <Trans i18nKey="home:bg-credits"
              components={[
                <a href="https://unsplash.com/photos/qqE7qERX7Go" target="_blank" rel="noopener" />,
                <a href="https://unsplash.com/" target="_blank" rel="noopener" />,
              ]}
              values={{
                name: "Henar Langa",
                site: "Unsplash"
              }}
            />
          </footer>
        </div>
      </main>
    </div>
  )
}
