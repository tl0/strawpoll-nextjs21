import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import useTranslation from 'next-translate/useTranslation'
import Trans from 'next-translate/Trans'
import styles from '../styles/Home.module.css'
import { getPolls } from '../database/pollModel'

export default function List({ polls }) {
    const router = useRouter()
    const { t, lang } = useTranslation('home')

    polls = JSON.parse(polls)

    return (
        <div className={styles.container}>
            <Head>
                <title>Recent polls created on Strawpoll.fi</title>
                <meta name="description" content='Recent polls created on Strawpoll.fi' key="description" />
                <meta name="robots" content='noindex,follow' key="robots" />
            </Head>

            <main className={styles.main}>
                <div className="w-full h-screen bg-no-repeat bg-fixed bg-cover blur bg-bottom">
                    <div className="w-full bg-opacity-50 dark:bg-opacity-80 bg-black flex justify-center items-center backdrop-blur-[3px] backdrop-filter">
                        <div className="w-full h-screen">
                            <div className="mx-4 text-center text-white dark:text-gray-300 lg:mt-16">
                                <h1 className="font-bold text-6xl mb-4">List of recent polls</h1>
                                <h2 className="font-bold text-3xl mb-12"></h2>
                                <div>
                                    <ul>
                                        {polls && polls.map((i) => (
                                            <li><Link href={`/r/${encodeURIComponent(i.id)}`}>
                                                {i.question}
                                            </Link></li>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}

export async function getStaticProps() {
    const polls = await getPolls()

    return {
        props: {
            polls: JSON.stringify(polls),
        },
        revalidate: 360,
    }
}
