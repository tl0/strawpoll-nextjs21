import React, { useState, useEffect, useRef } from "react";
import Head from "next/head";
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useForm } from "react-hook-form";
import fetch from 'node-fetch';

import styles from "../styles/Home.module.css";
import ToggleComponent from "../components/ToggleComponent";
import HoverTrigger from "../components/HoverTrigger";
import useTranslation from "next-translate/useTranslation";

const AlwaysScrollToBottom = () => {
  const elementRef: any = useRef();
  useEffect(() => elementRef?.current?.scrollIntoView());
  return <div ref={elementRef} />;
};

export default function New() {
  const { t, lang } = useTranslation('new')

  const { register: formRegister, handleSubmit: formHandleSubmit } = useForm()
  const [question, setQuestion] = useState("")
  const [answers, setAnswers] = useState(["", "", ""])
  const [loading, setLoading] = useState(false)

  const scrollRef: any = useRef();
  const router = useRouter()

  const addQ = () => {
    console.log("ADD");
    setAnswers((prevState) => [...answers, ""]);
    setTimeout(
      () =>
        scrollRef?.current?.scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "start",
        }),
      200
    );
  };

  const updateQ = (e) => {
    let tmp = [...answers];
    tmp[e.target.dataset["index"]] = e.target.value;
    setAnswers((prevState) => tmp);
    if (tmp[tmp.length - 1].length > 0) addQ();
  };

  const onSubmit = async (data) => {
    console.log(data);
    setLoading(true)
    try {

      const response = await fetch('/api/newPoll', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        }
      })
      let json = await response.json();
      let ls = JSON.parse(localStorage.getItem("createdPolls") || "[]")
      ls.push(json.id)
      localStorage.setItem("createdPolls", JSON.stringify(ls))
      console.log(json)
      router.push(`/p/${json.id}`)
    } catch (err) {
      console.error(err)
      setLoading(false)
    }
  };

  return (
    <div className="containe mx-auto">
      <Head>
        <title key="title">{t('h1')} - Strawpoll.fi</title>
        <meta name="description" content={t('description')} key="description" />
        <link rel="canonical" href={`https://strawpoll.fi/${router.locale !== router.defaultLocale ? router.locale + "/" : ''}new`} />
        <link rel="alternate" hrefLang="x-default" href="https://strawpoll.fi/en/new" />
        <link rel="alternate" hrefLang="fi" href="https://strawpoll.fi/new" />
        {router.locales.map(e => e !== router.defaultLocale && <link rel="alternate" hrefLang={e} href={`https://strawpoll.fi/${e}/new`} key={e} />)}
      </Head>

      <main className="{'mx-auto'}">
        <div className="flex relative items-top mx-auto justify-center min-h-screen items-center sm:pt-0">
          <div className="lg:mx-auto sm:px-1 lg:px-8 lg:mt-8 w-full md:w-2/3 lg:w-3/4 lg:max-w-4xl p-1 sm:p-0">
            <div className="lg:p-6 flex flex-col justify-center">
              <form onSubmit={formHandleSubmit(onSubmit)}>
                <div className="flex flex-col">
                  <h1 className="font-bold text-2xl mb-2 text-gray-900 dark:text-gray-100">{t('h1')}</h1>

                  <div className="mx-auto w-full mt-2 py-3 px-3 rounded-lg bg-white dark:bg-gray-800 border border-gray-400 dark:border-gray-700 text-gray-800 dark:text-gray-200 font-semibold focus:border-indigo-500 focus:outline-none">
                    <div className="md-input-box">
                      <input
                        {...formRegister('question', { required: true })}
                        id="question"
                        type="text"
                        className="md-input bg-transparent"
                        placeholder=" "
                        autoComplete="off"
                        defaultValue={question}
                        onChange={(e) => setQuestion(e.target.value)}
                        required
                      />
                      <label htmlFor="question" className="md-label">
                        {t('question')}
                      </label>
                      <div className="md-input-underline"></div>
                    </div>
                  </div>

                  <div className="w-full max-h-96 overflow-y-auto mt-2 py-3 px-3 rounded-lg bg-white dark:bg-gray-800 border border-gray-400 dark:border-gray-700 text-gray-800 dark:text-gray-200 font-semibold focus:border-indigo-500 focus:outline-none">
                    {answers.map((e, i) => {
                      return (
                        <div className="md-input-box pt-4" key={i}>
                          <input
                            {...formRegister(`choises[${i}]`)}
                            type="text"
                            className="md-input bg-transparent"
                            placeholder=""
                            autoComplete="off"
                            defaultValue={e}
                            onChange={updateQ}
                            data-index={i}
                            required={i <= 1}
                          />
                          <label
                            htmlFor={"answer" + i}
                            className="md-label"
                          >
                            {t('option')} {i + 1}
                          </label>
                          <div className="md-input-underline"></div>
                        </div>
                      );
                    })}
                    <div ref={scrollRef} />
                  </div>

                  <div className="w-full mt-2 py-3 px-3 rounded-lg bg-white dark:bg-gray-800 border border-gray-400 dark:border-gray-700 text-gray-800 dark:text-gray-200  focus:border-indigo-500 focus:outline-none">
                    <ToggleComponent value={t('captcha')} name="captcha" fref={formRegister} />
                    <ToggleComponent value={t('multi')} name="multiple" fref={formRegister} />
                    {/*<ToggleComponent value="Strictier voting manipulation prevent" name="strict" fref={formRegister} />
                        <HoverTrigger value="How dis works?">
                          <div className="">
                            <h3 className="text-blue-600 font-semibold text-lg">
                              Try to prevent automated answers
                              </h3>
                              <p>
                              We utilize various methods to prevent spamming,
                              botting or otherwise voting maliciously. Our
                              methods include, but are not limited to,
                              IP-address checking, cookies, known VPNs and
                              captcha.
                            </p>
                            <p className="text-red-500">
                            Nothing is 100% proof, but we try our best!
                            </p>
                            </div>
                        </HoverTrigger>*/}
                  </div>

                  <div className="flex flex-row gap-2">
                    <button
                      type="submit"
                      className="md:w-32 bg-indigo-600 hover:bg-blue-dark text-white font-bold py-3 px-6 rounded-lg mt-3 hover:bg-indigo-500 transition ease-in-out duration-300 disabled:opacity-50 disabled:bg-indigo-800"
                      disabled={loading}
                    >
                      {loading ?
                        <div className="loader ease-linear rounded-full border-4 border-t-4 border-gray-200 h-4 w-4 inline-block"></div> : <p>{t('submit')}</p>}
                    </button>
                    <Link href={'/'}>
                      <a
                        className="md:w-32 bg-green-400 hover:bg-blue-dark text-white font-bold py-3 px-6 rounded-lg mt-3 hover:bg-green-500 transition ease-in-out duration-300 text-center"
                      >
                        {t('cancel')}
                      </a>
                    </Link>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </main>
      <style jsx>
        {`
          .md-input-main {
            @apply font-sans text-xl w-full;
            width: 50%;
            font-size: 1.25rem;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
              "Helvetica Neue", Arial, "Noto Sans", sans-serif,
              "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol",
              "Noto Color Emoji";
          }
          .md-input-box {
            @apply relative;
            position: relative;
          }
          .md-input {
            @apply w-full;
            width: 100%;
            outline: none;
            height: 50px;
          }
          .md-label {
            @apply absolute pointer-events-none block;
            display: block;
            position: absolute;
            pointer-events: none;
            transform-origin: top left;
            transform: translate(0, -40px) scale(1);
            transition: color 200ms cubic-bezier(0, 0, 0.2, 1) 0ms,
              transform 200ms cubic-bezier(0, 0, 0.2, 1) 0ms;
          }
          .md-label-focus {
            @apply text-blue;
            color: #3182ce;
            transform: translate(0, -65px) scale(0.75);
            transform-origin: top left;
          }
          .md-input-underline {
            border-bottom: 1px solid #718096;
          }
          .md-input-underline:after {
            @apply absolute left-0 right-0 pointer-events-none;
            position: absolute;
            left: 0;
            right: 0;
            pointer-events: none;
            bottom: -0.05rem;
            content: "";
            transition: transform 200ms cubic-bezier(0, 0, 0.2, 1) 0ms;
            transform: scaleX(0);
            border-bottom: 2px solid #805ad5;
          }
          .md-input:focus ~ .md-input-underline:after {
            transform: scaleX(1);
          }
          .md-input:focus + .md-label,
          .md-input:not(:placeholder-shown) + .md-label {
            @apply md-label-focus;
            color: #3182ce;
            transform: translate(0, -65px) scale(0.75);
            transform-origin: top left;
          }
          .loader {
            border-top-color: #3498db;
            -webkit-animation: spinner 1.5s linear infinite;
            animation: spinner 1.5s linear infinite;
          }

          @-webkit-keyframes spinner {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
          }

          @keyframes spinner {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
          }
        `}
      </style>
    </div >
  );
}
