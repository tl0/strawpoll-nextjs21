import React, { useState, useEffect, useRef } from "react";
import { GetStaticProps, GetStaticPaths } from 'next'
import Head from "next/head";
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useForm } from "react-hook-form";
import fetch from 'node-fetch';
import HCaptchaWrapper from '../../components/HCaptchaWrapper';
import { getPoll } from '../../database/pollModel'
import useTranslation from "next-translate/useTranslation";

export default function New(props) {
    const { t, lang } = useTranslation('vote')

    const { register: formRegister, handleSubmit: formHandleSubmit, setValue } = useForm();
    const [loading, setLoading] = useState(false)

    const router = useRouter()

    const poll = JSON.parse(props.poll)
    const { ref: captchaRef } = formRegister('captcha', { required: poll.options.captcha })
    setValue('id', poll.id)

    const [lsCreated, setLsCreated] = useState([])
    useEffect(() => {
        setLsCreated(JSON.parse(localStorage.getItem("createdPolls") || "[]"))
        console.log(lsCreated)
    }, [])

    const onSubmit = async (data) => {
        setLoading(true)
        try {
            const response = await fetch('/api/vote', {
                method: 'POST',
                body: JSON.stringify({ id: poll.id, choise: data.choise, captcha: data.captcha }),
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            let json = await response.json();
            router.push(`/r/${poll.id}`)
        } catch (err) {
            console.error(err)
            setLoading(false)
        }
    };

    const onCaptchaVerify = e => {
        setValue('captcha', e)
    }

    const onCaptchaError = e => {
        console.log(e)
    }
    const onCaptchaExpire = e => {
        console.log(e)
    }

    const tryDelete = async (data) => {
        try {
            const response = await fetch('/api/edit', {
                method: 'POST',
                body: JSON.stringify({ id: poll.id, action: "delete" }),
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            let json = await response.json();
            localStorage.setItem("createdPolls", JSON.stringify(lsCreated.filter(i => i !== poll.id)))
            if (json.result === "rm ok") {
                console.log("Removed!")
                router.replace(`/`)
            } else {
                console.error("IP didn't match or something went wrong.")
            }
        } catch (err) {
            console.error(err)
        }
    }

    return (
        <div className="container mx-auto">
            <Head>
                <title key="title">{poll.question.substring(0, 20)} - Strawpoll.fi</title>
                <meta name="description" content={t('description', { question: poll?.question })} key="description" />
                <link rel="canonical" href={`https://strawpoll.fi/${router.locale !== router.defaultLocale ? router.locale + "/" : ''}p/${encodeURIComponent(props.pid[0])}`} />
                <link rel="alternate" hrefLang="x-default" href={`https://strawpoll.fi/en/p/${encodeURIComponent(props.pid[0])}`} />
                <link rel="alternate" hrefLang="fi" href={`https://strawpoll.fi/p/${encodeURIComponent(props.pid[0])}`} />
                {router.locales.map(e => e !== router.defaultLocale && <link rel="alternate" hrefLang={e} href={`https://strawpoll.fi/${e}/p/${encodeURIComponent(props.pid[0])}`} key={e} />)}
            </Head>

            <main className="{styles.main}">
                <div className="flex items-top justify-center min-h-screen sm:items-center sm:pt-0">
                    <div className="mx-auto lg:w-2/4 sm:px-1 lg:px-8">
                        <div className="mt-8">
                            <div className="lg:p-6 flex flex-col justify-center">
                                <form onSubmit={formHandleSubmit(onSubmit)}>
                                    <div className="flex flex-col">
                                        <h1 className="text-gray-800 dark:text-gray-100 font-semibold mt-1">{t('h1')}</h1>
                                        <h2 className="w-full mt-2 py-3 px-3 rounded-lg bg-white dark:bg-gray-800 border border-gray-400 dark:border-gray-700 text-gray-800 dark:text-gray-200 font-semibold focus:border-indigo-500 focus:outline-none" lang="">
                                            {poll.question}
                                        </h2>

                                        <div className="w-full max-h-96 overflow-y-auto mt-2 py-3 px-3 rounded-lg bg-white dark:bg-gray-800 border border-gray-400 dark:border-gray-700 text-gray-800 font-semibold focus:border-indigo-500 focus:outline-none">
                                            {poll.choises.map((e, i) => {
                                                return (
                                                    <div key={e._id}>
                                                        <label className="inline-flex items-center">
                                                            {poll.options.multiple ?
                                                                <input type="checkbox" id={e.id} value={e.id} name="choise" {...formRegister("choise")} />
                                                                :
                                                                <input type="radio" className="form-radio" id={e.id} value={e.id} name="choise" {...formRegister("choise")} />
                                                            }
                                                            <span className="ml-2 dark:text-gray-200" lang="">{e.text}</span>
                                                        </label>

                                                    </div>
                                                );
                                            })}
                                        </div>

                                        {poll.options.captcha &&
                                            <div>
                                                <HCaptchaWrapper
                                                    ref={captchaRef}
                                                    id="captcha"
                                                    sitekey={process.env.NEXT_PUBLIC_HCAPTCHA_SITEKEY}
                                                    onVerify={onCaptchaVerify}
                                                    onError={onCaptchaError}
                                                    onExpire={onCaptchaExpire}
                                                    theme={typeof window !== 'undefined' && window?.matchMedia('(prefers-color-scheme: dark)').matches ? "dark" : ""}
                                                    size=""
                                                />
                                            </div>
                                        }

                                        <div className="flex flex-row gap-2">
                                            <button
                                                type="submit"
                                                className="md:w-32 bg-indigo-600 hover:bg-blue-dark text-white font-bold py-3 px-6 rounded-lg mt-3 hover:bg-indigo-500 transition ease-in-out duration-300"
                                                disabled={loading}
                                                name="submit"
                                            >
                                                {loading ?
                                                    <div className="loader ease-linear rounded-full border-4 border-t-4 border-gray-200 h-4 w-4 inline-block"></div> : <p>{t('submit')}</p>}
                                            </button>

                                            <Link href={`/r/${encodeURIComponent(props.pid[0])}`}>
                                                <a
                                                    className="md:w-32 bg-green-400 hover:bg-blue-dark text-white font-bold py-3 px-6 rounded-lg mt-3 hover:bg-green-500 transition ease-in-out duration-300 text-center"
                                                >
                                                    {t('results')}
                                                </a>
                                            </Link>

                                            <Link href={'/'}>
                                                <a
                                                    className="md:w-36 bg-pink-400 hover:bg-blue-dark text-white font-bold py-3 px-6 rounded-lg mt-3 hover:bg-pink-500 transition ease-in-out duration-300 text-center"
                                                >
                                                    {t('create')}
                                                </a>
                                            </Link>
                                        </div>
                                        {lsCreated.includes(props.pid[0]) &&
                                            <div className="flex gap-2 border-t-2 mt-4">
                                                <button
                                                    type="button"
                                                    onClick={tryDelete}
                                                    className="bg-red-400 hover:bg-blue-dark text-white font-bold py-3 px-6 rounded-lg mt-3 hover:bg-red-500 transition ease-in-out duration-300 text-center"
                                                >
                                                    ❌
                                                </button>
                                            </div>
                                        }
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <style jsx>
                {`.loader {
            border-top-color: #3498db;
            -webkit-animation: spinner 1.5s linear infinite;
            animation: spinner 1.5s linear infinite;
          }

          @-webkit-keyframes spinner {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
          }

          @keyframes spinner {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
          }`}
            </style>

        </div >
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    let tmp;
    try {
        tmp = await getPoll({
            id: context.params.pid[0],
        });

        if (!tmp) {
            return {
                redirect: {
                    destination: '/',
                    permanent: false,
                },
            }
        }

        return {
            props: {
                pid: context.params.pid,
                poll: JSON.stringify(tmp),
            },
            revalidate: 300,
        }
    } catch (err) {
        console.error(err)
        /*return {
                redirect: {
                destination: '/',
                permanent: false,
            },
        }*/
        return {
            notFound: true,
        }
    }
}

export const getStaticPaths: GetStaticPaths = async (context) => {
    return {
        paths: [{ params: { pid: ["demo-1234"] } }],
        fallback: 'blocking',
    }
}
