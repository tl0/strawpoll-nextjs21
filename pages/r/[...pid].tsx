import Head from 'next/head'
import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { ResponsivePie } from '@nivo/pie'
import useSWR from 'swr'
import { getPoll } from '../../database/pollModel'
import useTranslation from "next-translate/useTranslation";
import { GetStaticProps, GetStaticPaths } from 'next'

const MyResponsivePie = ({ data }) => (
    <ResponsivePie
        data={data}
        margin={{ top: 10, right: 10, bottom: 10, left: 10 }}
        innerRadius={0.420}
        padAngle={1}
        cornerRadius={5}
        colors={{ scheme: 'nivo' }}
        borderWidth={1}
        borderColor={{ from: 'color', modifiers: [['darker', 0.2]] }}
        enableArcLinkLabels={false}
        arcLabel="label"
        arcLinkLabelsSkipAngle={10}
        arcLinkLabelsTextColor="#333333"
        arcLinkLabel={function (e) { return e.label + " (" + e.value + ")" }}
        tooltip={i => (
            <div className="bg-white p-1 rounded-sm">
                <b>{i.datum.label}</b> ({i.datum.value})
            </div>
        )}
    />
)

const fetcher = url => fetch(url).then(r => r.json())

const Poll = (props) => {
    const { t, lang } = useTranslation('result')

    const { data, error } = useSWR(() => '/api/getPoll?id=' + props.pid[0], fetcher, {
        initialData: JSON.parse(props.poll),
        refreshInterval: 2000,
        onSuccess: (data, key, config) => {
            console.info("[SWR] onSuccess")
            //console.log(data)
        },
        onError: (err, key, config) => {
            console.log(err)
        },
        compare: (a, b) => {
            if (a === undefined || b === undefined) {
                console.error("[SWR] a or b is undefined??")
                return false
            }
            return a.totalVotes > b.totalVotes
        }
    })

    const router = useRouter()
    const { pid } = router.query

    //if (error) return <div>ERROR</div>
    if (!data) return <div>loading</div>

    return (
        <div className="container mx-auto">
            <Head>
                <title key="title">{data.question.substring(0, 20)} - Strawpoll.fi</title>
                <meta name="description" content={t('description', { question: data?.question })} key="description" />
                <link rel="canonical" href={`https://strawpoll.fi/${router.locale !== router.defaultLocale ? router.locale + "/" : ''}r/${encodeURIComponent(props.pid[0])}`} />
                <link rel="alternate" hrefLang="x-default" href={`https://strawpoll.fi/en/r/${encodeURIComponent(props.pid[0])}`} />
                <link rel="alternate" hrefLang="fi" href={`https://strawpoll.fi/r/${encodeURIComponent(props.pid[0])}`} />
                {router.locales.map(e => e !== router.defaultLocale && <link rel="alternate" hrefLang={e} href={`https://strawpoll.fi/${e}/r/${encodeURIComponent(props.pid[0])}`} key={e} />)}
            </Head>

            <main className="{styles.main}">
                <div className="relative flex items-top justify-center min-h-screen sm:items-center sm:pt-0">
                    <div className="mx-auto w-full sm:w-2/3 md:w-3/4 lg:w-7/12 sm:px-6 lg:px-8">
                        <div className="mt-8 overflow-hidden">
                            <div className="grid grid-cols-1 md:grid-cols-2">
                                <div className="shadow-lg rounded-lg px-4 py-4">
                                    <div className="mb-1 tracking-wide px-4 py-4">
                                        <h1 className="text-gray-600 dark:text-gray-300 font-semibold mt-1">{t('h1')}</h1>
                                        <h2 className="text-gray-600 dark:text-gray-300 font-semibold mt-1" lang="">{data?.question}</h2>
                                        <div className="border-b -mx-8 px-8 pb-3">
                                            {data?.choises?.map((e => (
                                                <div className="flex flex-wrap items-center mt-1" key={e._id} title={`${e.votes} voted for this`}>
                                                    <div className="min-w-[150px] flex-auto w-1/3 text-indigo-500 tracking-tighter">
                                                        <span lang="">{e.text}</span>
                                                    </div>
                                                    <div className="w-2/3 flex-auto p-1">
                                                        <div className="bg-gray-400 w-full rounded-lg h-2">
                                                            <div className="bg-indigo-700 rounded-lg h-2" style={{ width: (data.totalVotes <= 0) ? 0 : e.votes / data.totalVotes * 100 + '%' }} />
                                                        </div>
                                                        <span className="text-gray-700 dark:text-gray-300 w-0 text-sm">{t('votes', { count: e.votes })} &ndash; {(data.totalVotes <= 0) ? 0 + '%' : Number(e.votes / data.totalVotes).toLocaleString('en-GB', { style: 'percent', maximumFractionDigits: 2 })}</span>
                                                    </div>
                                                </div>
                                            )))}
                                            <h2 className="text-gray-700 dark:text-gray-300 font-semibold mt-1">{t('votes', { count: data.totalVotes })}</h2>
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">

                                        <div className="w-full px-4">
                                            <h3 className="font-medium tracking-tight text-gray-800 dark:text-gray-100">{t('vote')}</h3>
                                            <p className="text-gray-700 dark:text-gray-300 text-sm py-1">{t('vote2')}</p>
                                            <Link href={`/p/${encodeURIComponent(props.pid[0])}`}>
                                                <a className="bg-gray-100 dark:bg-gray-900 border border-gray-400 px-3 m-3 py-1 rounded text-gray-700 dark:text-gray-300">{t('votebtn')}</a>
                                            </Link>
                                        </div>
                                        <div className="w-full px-4">
                                            <h3 className="font-medium tracking-tight text-gray-800 dark:text-gray-100">{t('create')}</h3>
                                            <p className="text-gray-700 dark:text-gray-300 text-sm py-1">{t('create2')}</p>
                                            <Link href={'/'}>
                                                <a className="bg-gray-100 dark:bg-gray-900 border border-gray-400 px-3 py-1 rounded text-gray-700 dark:text-gray-300 mt-2">{t('createbtn')}</a>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className=" shadow-lg rounded-lg p-2">
                                    <div className="mb-1 tracking-wide h-96 w-fill">
                                        <MyResponsivePie data={
                                            data?.choises?.map(e => ({
                                                "id": e._id,
                                                "label": e.text,
                                                "value": e.votes,
                                            })) || {}
                                        } />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>

    )
}

export default Poll

export const getStaticProps: GetStaticProps = async (context) => {
    let t;
    try {
        t = await getPoll({
            id: context.params.pid[0],
        });

        console.log(t)
        if (!t) {
            return {
                redirect: {
                    destination: '/',
                    permanent: false,
                },
            }
        }

        return {
            props: {
                pid: context.params.pid,
                poll: JSON.stringify(t),
            },
            revalidate: 10,
        }
    } catch (err) {
        console.error(err)
        /*return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }*/
        return {
            notFound: true,
        }
    }
}

export const getStaticPaths: GetStaticPaths = async (context) => {
    return {
        paths: [{ params: { pid: ["demo-1234"] } }],
        fallback: 'blocking',
    }
}
